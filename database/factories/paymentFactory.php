<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Payment::class, function (Faker $faker) {
    return [
        'user_id'=>rand(1,5),
        'resnumber'=>rand(1000000000,9000000000),
        'price'=>rand(50000,500000),
        'course_id'=>rand(1,3),
        'status'=>$faker->boolean,
        'created_at'=>$faker->dateTimeBetween('-5 months','now')
    ];
});

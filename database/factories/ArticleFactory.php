<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Article;
use App\Model\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'category_name' => $faker->title,
        'subject' => 'course',
        'visibility' => 'show'
    ];
});
$factory->define(Article::class, function (Faker $faker) {
    return [
        'user_id'=>rand(1,5),
        'category_id'=>rand(1,5),
        'title' => $faker->title,
        'description' => $faker->sentence(5),
        'body' => $faker->paragraph(5),
        'tag' => 'tag1,tag2,tag3',
        'imgsUrl' => [
            'thumb' => $faker->imageUrl(),
            'images' => [
                '600' => $faker->imageUrl(600, 600),
                'original' => $faker->image()

            ]
        ],
        'time' => $faker->randomNumber(),
        'lang' => 'fa',

    ];
});

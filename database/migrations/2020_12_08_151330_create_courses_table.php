<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->string('title');
            $table->string('description');
            $table->text('body')->nullable();
            $table->string('price',7)->nullable()->default(0);
            $table->string('type',10)->default('free');
            $table->string('tag')->nullable();
            $table->string('slug');
            $table->text('imgUrl')->nullable();
            $table->string('viewCount')->default(0);
            $table->integer('commentCount')->unsigned()->default(0);
            $table->string('episodeCount')->default(0);
            $table->string('time')->default('00:00:00');
            $table->string('lang',5)->default('fa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}

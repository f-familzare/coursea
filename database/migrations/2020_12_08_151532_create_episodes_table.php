<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('course_id');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('type',10)->default('free');
            $table->string('tag')->nullable();
            $table->string('slug');
            $table->string('fileUrl')->nullable();
            $table->integer('number')->default(0);
            $table->string('downloadCount')->default(0);
            $table->integer('commentCount')->unsigned()->default(0);
            $table->string('viewCount')->default(0);
            $table->string('time')->default('00:00:00');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episodes');
    }
}

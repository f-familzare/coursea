<?php

use App\Model\Article;
use App\Model\Permission;
use App\Model\Role;
use App\Model\Payment;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(Permission::class, 10)->create();
        factory(Role::class, 4)->create();
        factory(\App\User::class, 5)->create();
        factory(Payment::class, 30)->create();
        factory(\App\Model\Category::class, 5)->create();
        factory(Article::class, 5)->create();
    }
}

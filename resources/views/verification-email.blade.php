@component('mail::message')
    <body dir="rtl" style="text-align: center;">
    <h3> به وب سایت آموزشی Coursea خوش آمدید ... </h3>
    <p>برای فعالسازی روی دکمه زیر کلیک کنید.</p>
    @component('mail::button', ['url' => route('active.email',['token'=>$code]) , 'color' => 'primary'])
        فعالسازی حساب کاربری
    @endcomponent
    </body>
@endcomponent

@component('UserPanel.master')

    <div style="margin: 20px;">
        <form action="{{ route('user.panel.payment') }}" method="post">
            {{ csrf_field() }}
            <select name="plan">
                <option value="3">عضویت ویژه 1 ماه 1000 تومان</option>
                <option value="6">عضویت ویژه 3 ماه 3000 تومان</option>
                <option value="12">عضویت ویژه 12 ماه 12000 تومان</option>
            </select>
            <button type="submit">افزایش اعتبار</button>
        </form>
    </div>

@endcomponent

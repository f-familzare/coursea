@extends('layouts.app')
@section('content')
    <div class="container  my-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <img src="/panel/img/avatar-icon-3.png" width="250" height="250" alt="" class="academyitLogo"/>
                        <form class="form-signin" method="POST" action="{{ route('login') }}" id="demo-form">
                            @include('errors.errors')
                            @csrf
                            <h2 class="form-signin-heading">همین حالا وارد شوید</h2>
                            <div class="login-wrap">
                                <input type="text" class="form-control @error('email') is-invalid @enderror"
                                       placeholder="{{ __('نام کاربری (ایمیل)')}}" name="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                       name="password" placeholder="{{ __('کلمه عبور') }}"
                                       autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label class="checkbox">
                                    <input type="checkbox" value="remember-me"
                                           name="remember" {{ old('remember') ? 'checked' : '' }}> <a
                                        href="{{ route('password.request') }}"> کلمه عبور را فراموش کرده اید؟</a>
                                    @if(Route::has('password.request'))
                                        <span class="pull-right"> مرا به خاطر بسپار</span>
                                    @endif
                                </label>
                                <button class="g-recaptcha btn btn-lg btn-login btn-block"
                                        data-sitekey="{{env('reCAPTCHA_Site_Key')}}"
                                        data-callback='onSubmit'
                                        data-action="submit" type="submit">ورود
                                </button>
                                <div class="text-center">
                                    <a href="{{route('login.google')}}" class="btn btn-lg btn-login"> ورود با <i
                                            class="icon-google-plus"></i> </a>
                                    <a href="{{route('login.github')}}" class="btn btn-lg btn-login"> ورود با <i
                                            class="icon-github"></i> </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        function onSubmit(token) {
            document.getElementById("demo-form").submit();
        }
    </script>
@endsection



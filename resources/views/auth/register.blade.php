@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <img src="/panel/img/avatar-icon-3.png" width="250" height="250" alt="" class="academyitLogo"/>
                        <form method="POST" action="{{ route('register') }}" class="form-signin">
                            @csrf
                            <h2 class="form-signin-heading">همین حالا عضو شوید</h2>
                            <div class="login-wrap">
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                       placeholder="{{__('نام')}}" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input type="text" name="last_name"
                                       class="form-control @error('last_name') is-invalid @enderror"
                                       placeholder="{{__('نام خانوادگی')}}" autofocus>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input type="text" class="form-control @error('email') is-invalid @enderror"
                                       placeholder="{{ __('ایمیل (نام کاربری) ') }}" name="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                       name="password" placeholder="{{ __('کلمه عبور') }}"
                                       autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input type="password" class="form-control" placeholder="{{ __('تکرار کلمه عبور') }}"
                                       name="password_confirmation" required autocomplete="new-password">
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label class="checkbox">
                                    <input type="checkbox" value="remember-me"
                                           name="remember" {{ old('remember') ? 'checked' : '' }}> <a
                                            href="{{ route('password.request') }}"> کلمه عبور را فراموش کرده اید؟</a>
                                    @if(Route::has('password.request'))
                                        <span class="pull-right"> مرا به خاطر بسپار</span>
                                    @endif
                                </label>
                                <button class="btn btn-lg btn-login btn-block" id="login" type="submit" name="btn">ثبت نام</button>
                                <div class="text-center">
                                    <a href="{{url('login/google')}}" class="btn btn-lg btn-login">  ثبت نام با <i class="icon-google-plus"></i> </a>
                                    <a href="{{url('login/github')}}" class="btn btn-lg btn-login"> ثبت نام با <i class="icon-github"></i> </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




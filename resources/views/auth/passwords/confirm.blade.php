@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form class="form-signin" method="POST" action="{{route('password.confirm')}}">
                        @csrf
                        <h2 class="form-signin-heading">لطفا رمزعبور خود را مجددا وارد کنید</h2>
                        <div class="login-wrap">
                            <input type="text" class="form-control @error('password') is-invalid @enderror"
                                   placeholder="{{ __('رمزعبور') }}" name="password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror


                            <label class="checkbox">
                                <input type="checkbox" value="remember-me"
                                       name="remember" {{ old('remember') ? 'checked' : '' }}> <a href="{{ route('password.request') }}"> کلمه عبور را فراموش کرده اید؟</a>
                                @if(Route::has('password.request'))
                                    <span class="pull-right"> مرا به خاطر بسپار</span>
                                @endif
                            </label>
                            <button class="btn btn-lg btn-login btn-block" type="submit" name="btn">تایید رمزعبور</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <img src="/panel/img/avatar-icon-3.png" width="250" height="250" alt="" class="academyitLogo"/>
                        <form class="form-signin" method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <h2 class="form-signin-heading">رمزعبور خود را فراموش کرده اید؟</h2>
                            <div class="login-wrap">
                                <input type="text" class="form-control @error('email') is-invalid @enderror"
                                       placeholder="{{ __('ایمیل ') }}" name="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('ارسال لینک بازیابی رمزعبور') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

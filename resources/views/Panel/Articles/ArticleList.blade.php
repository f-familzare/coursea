@extends('layouts.panel-master')
@section('title','لیست محصولات')
@section('content')
    <!-- page start-->
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست مقالات
                <a href="{{route('articles.create')}}" class="pull-left btn-sm btn-default">مقاله جدید</a>
            </header>
            <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-list"></i></th>
                        <th>عنوان مقاله</th>
                        <th>نویسنده</th>
                        <th><i class="icon-eye-open"></i> بازدید</th>
                        <th><i class="icon-comments"></i> نظرات</th>
                        <th class="hidden-phone"><i class="icon-file-text"></i> متن کوتاه</th>
                        <th><i class="icon-stop"></i> وضعیت</th>
                        <th>تاریخ ایجاد</th>
                        <th>تاریخ بروزرسانی</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($articles as $article)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="{{ $article->path()}}">{{$article->title}}</a>
                            </td>
                            <td><a href="#">{{$article->user->name.' '.$article->user->name}}</a></td>
                            <td>{{$article->viewCount}}</td>
                            <td>{{$article->commentCount}}</td>
                            <td>
                                <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-warning"><i class="icon-file-text-alt"></i></a>
                            </td>

                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                            <h4 class="modal-title">{{$article->title}} (  زمان مطالعه : {{$article->time}} دقیقه ) </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="card">
                                                    <div class="card-body">
                                                        {{$article->description}}</div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <td>
                                <span class="label {{$article->visibility=='show'?'label-success':'label-danger'}}">
                                    <i class="{{$article->visibility=='show'?'icon-eye-open':'icon-eye-close'}}"></i>
                                </span>
                            </td>
                            <td>{{jdate($article->created_at)->format('%A, %d %B %y')}}</td>
                            <td class="hidden-phone">{{jdate($article->updated_at)->format('%A, %d %B %y')}}</td>
                            <td>
                                <a href="{{route('articles.edit',['article'=>$article->id])}}"
                                   class="btn btn-primary btn-xs"><i
                                            class="icon-pencil"></i></a>
                                <form action="{{route('articles.destroy',['article'=>$article->id])}}" method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$articles->links()}}
        </section>
    </div>
    <!-- page end-->

    <!-- js placed at the end of the document so the pages load faster -->

    <!--script for this page only-->
    <script src="/Panel/js/dynamic-table.js"></script>
@endsection

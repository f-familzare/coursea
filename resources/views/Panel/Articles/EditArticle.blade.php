@extends('layouts.panel-master')
@section('title','ویرایش مقاله')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            ویرایش مقاله
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST"
                  action="{{route('articles.update',['article'=>$article->id])}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label">عنوان مقاله :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="title" value="{{$article->title}}">
                    </div>

                    <label class="col-sm-2 control-label">برچسب های مقاله :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="tag" value="{{$article->tag}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">تصویر مقاله :</label>
                    <div class="col-sm-4">
                        <input type="file" class="form-control round-input" name="imgsUrl">
                    </div>

                    <label class="col-sm-2 control-label">مدت مطالعه :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="time" value="{{$article->time}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    @if($article->imgsUrl)
                        @foreach($article->imgsUrl['imgsUrl'] as $key=>$image)
                            <label class="control-label" for="{{$key}}">{{$key}}
                                <div class="col-xs-6">
                                    <input type="radio" value="{{$article->imgsUrl['imgsUrl']['original']}}"
                                           name="thumbNail" {{$article->imgsUrl['thumb']==$image?'checked':''}}>
                                    <a href="{{$image}}">
                                        <img src="{{$image}}" alt="{{$article->title}}" class="task-thumb img-rounded">
                                    </a>
                                </div>
                            </label>
                        @endforeach
                    @else
                        <div class="alert alert-info"> تصویری برای مقاله تعیین نشده است.</div>
                    @endif
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">توضیحات کوتاه :</label>
                    <div class="col-sm-10">
                    <textarea name="description" id="" cols="85" rows="4"
                              class="form-control">{{$article->description}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">توضیحات : </label>
                </div>
                <div class="form-group">
                <textarea name="body" id="body" cols="30" rows="10"
                          class="form-control col-sm-10">{{$article->body}}</textarea>
                    <label class="col-sm-2 control-label">وضعیت :</label>
                    <div class="col-sm-4">
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" class="btn btn-success" name="visibility" id="visibility1"
                                       value="show"
                                        {{$article->visibility=='show'?'checked':''}}>
                                <button type="button" class="btn btn-success btn-xs"><i class="icon-eye-open"></i>
                                </button>
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" name="visibility" id="visibility2"
                                       value="hidden" {{$article->visibility=='hidden'?'checked':''}}>
                                <button type="button" class="btn btn-danger btn-xs"><i class="icon-eye-close"></i>
                                </button>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn btn-round btn-primary">ویرایش</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>

        CKEDITOR.replace('body', {
            filebrowserUploadUrl: '/admin/upload-image',
            filebrowserImageUploadUrl: '/admin/upload-image',
        });
    </script>
@endsection

@extends('layouts.panel-master')
@section('title','افزودن سمت')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            افزودن سمت جدید
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST" action="{{route('roles.store')}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label">عنوان سمت :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="role_name"
                               value="{{old('role_name')}}">
                    </div>

                    <label class="col-sm-2 control-label"> دسترسی ها :</label>
                    <div class="col-sm-4">
                        <select name="permissions[]" class="form-control round-input permissions" multiple>
                            <option value="0" disabled>انتخاب کنید ...</option>
                            @foreach($permissions as $permission)
                                <option value="{{$permission->id}}">{{$permission->permission_name}}
                                    - {{$permission->label}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">توضیحات کوتاه :</label>
                    <div class="col-sm-4">
                        <textarea class="form-control round-input" name="label" id="" cols="30"
                                  rows="5">{{old('label')}}</textarea>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn btn-round btn-primary">ذخیره</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="/panel/js/bootstrap-select.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.permissions').selectpicker();
        })
    </script>
@endsection


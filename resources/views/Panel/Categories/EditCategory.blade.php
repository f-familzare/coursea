@extends('layouts.panel-master')
@section('title','ویرایش دسته بندی')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            ویرایش دسته بندی
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST"
                  action="{{route('categories.update',['category'=>$category->id])}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label">عنوان دسته بندی :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="category_name"
                               value="{{$category->category_name}}">
                    </div>

                    <label class="col-sm-2 control-label">دسته بندی مادر :</label>
                    <div class="col-sm-4">
                        <select name="parent_id" class="form-control round-input roles">
                            <option value="0" selected>دسته بندی مادر</option>
                            @foreach($categories as $cat)
                                <option
                                    value="{{$cat->id}}" {{$cat->id==$category->parent_id?'selected':''}}>{{$cat->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">وضعیت :</label>
                    <div class="col-sm-4">
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" class="btn btn-success" name="visibility" id="visibility1"
                                       value="show"
                                    {{$category->visibility=='show'?'checked':''}}>
                                <button type="button" class="btn btn-success btn-xs"><i class="icon-eye-open"></i>
                                </button>
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" name="visibility" id="visibility2"
                                       value="hidden" {{$category->visibility=='hidden'?'checked':''}}>
                                <button type="button" class="btn btn-danger btn-xs"><i class="icon-eye-close"></i>
                                </button>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn btn-round btn-primary">ویرایش</button>
                </div>
            </form>
        </div>
    </section>
@endsection

@extends('layouts.panel-master')
@section('title','افزودن دسته بندی')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            افزودن دسته بندی جدید
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST" action="{{route('categories.store')}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label">عنوان دسته بندی :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="category_name"
                               value="{{old('category_name')}}">
                    </div>


                    <label class="col-sm-2 control-label">دسته بندی مادر :</label>
                    <div class="col-sm-4">
                        <select name="parent_id" class="form-control round-input roles">
                            <option value="0" selected>دسته بندی مادر</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">نوع دسته بندی :</label>
                    <div class="col-sm-4">
                        <select name="subject" class="form-control round-input roles">
                            <option value="0" disabled>انتخاب کنید ... </option>
                            <option value="course">دوره ها</option>
                            <option value="article">مقالات</option>

                        </select>
                    </div>
                    <label class="col-sm-2 control-label">وضعیت :</label>
                    <div class="col-sm-4">
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" class="btn btn-success" name="visibility" id="visibility1"
                                       value="show"
                                       checked>
                                <button type="button" class="btn btn-success btn-xs"><i class="icon-eye-open"></i>
                                </button>
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" name="visibility" id="visibility2" value="hidden">
                                <button type="button" class="btn btn-danger btn-xs"><i class="icon-eye-close"></i>
                                </button>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn btn-round btn-primary">ذخیره</button>
                </div>
            </form>
        </div>
    </section>
@endsection



@extends('layouts.panel-master')
@section('title','لیست دسته بندی ها')
@section('content')
    <!-- page start-->
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست دسته بندی ها
                <a href="{{route('categories.create')}}" class="pull-left btn-sm btn-default">دسته بندی جدید</a>
            </header>
            <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-list"></i></th>
                        {{--                        <th>تصویر</th>--}}
                        <th>نام دسته بندی</th>
                        <th>دسته بندی مادر</th>
                        <th>نوع دسته بندی</th>
                        <th><i class="icon-eye-open"></i> وضعیت</th>
                        <th>تاریخ ایجاد</th>
                        <th>تاریخ بروزرسانی</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$i++}}</td>

                            <td>
                                <a href="{{$category->path()}}">{{$category->category_name}}</a>
                            </td>
                            <td>
                                {{$category->parent_id>0?$category->parent->category_name:'-'}}
                            </td>
                            <td>{{$category->subject}}</td>
                            <td>
                                <span class="label {{$category->visibility=='show'?'label-success':'label-danger'}}">
                                    <i class="{{$category->visibility=='show'?'icon-eye-open':'icon-eye-close'}}"></i>
                                </span>
                            </td>
                            <td>{{jdate($category->created_at)->format('%A, %d %B %y')}}</td>
                            <td class="hidden-phone">{{jdate($category->updated_at)->format('%A, %d %B %y')}}</td>
                            <td>
                                <a href="{{route('categories.edit',['category'=>$category->id])}}"
                                   class="btn btn-primary btn-xs"><i
                                        class="icon-pencil"></i></a>
                                <form action="{{route('categories.destroy',['category'=>$category->id])}}" method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$categories->links()}}
        </section>
    </div>
    <!-- page end-->

    <!-- js placed at the end of the document so the pages load faster -->

    <!--script for this page only-->
    <script src="/Panel/js/dynamic-table.js"></script>
@endsection

@extends('layouts.panel-master')
@section('title','لیست دسترسی ها')
@section('content')
    <!-- page start-->
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست دسترسی ها
                <a href="{{route('permissions.create')}}" class="pull-left btn-sm btn-default">دسترسی جدید</a>
            </header>
            <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-list"></i></th>
                        <th>عنوان دسترسی </th>
                        <th><i class="icon-file-text"></i> توضیحات کوتاه </th>
                        <th>تاریخ ایجاد</th>
                        <th>تاریخ بروزرسانی</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($permissions as $permission)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$permission->permission_name}}</td>
                            <td><a href="#">{{$permission->label}}</a></td>
                            <td>{{jdate($permission->created_at)->format('%A, %d %B %y')}}</td>
                            <td class="hidden-phone">{{jdate($permission->updated_at)->format('%A, %d %B %y')}}</td>
                            <td>
                                <a href="{{route('permissions.edit',['permission'=>$permission->id])}}"
                                   class="btn btn-primary btn-xs"><i
                                            class="icon-pencil"></i></a>
                                <form action="{{route('permissions.destroy',['permission'=>$permission->id])}}" method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$permissions->links()}}
        </section>
    </div>
    <!-- page end-->

    <!-- js placed at the end of the document so the pages load faster -->

    <!--script for this page only-->
    <script src="/Panel/js/dynamic-table.js"></script>
@endsection
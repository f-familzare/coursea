@extends('layouts.panel-master')
@section('title','ویرایش دسترسی')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            ویرایش دسترسی {{$permission->permission_name}}
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST" action="{{route('permissions.update',['permission'=>$permission->id])}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label">عنوان دسترسی :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="permission_name"
                               value="{{$permission->permission_name}}">
                    </div>
                    <label class="col-sm-2 control-label">توضیحات کوتاه :</label>
                    <div class="col-sm-4">
                        <textarea class="form-control round-input" name="label" id="" cols="30"
                                  rows="5">{{$permission->label}}</textarea>
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="text-center">
                    <button class="btn btn-round btn-primary">ویرایش</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="/panel/js/bootstrap-select.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.permissions').selectpicker();
        })
    </script>
@endsection


@extends('layouts.panel-master')
@section('title','ویرایش سطح دسترسی کاربر')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            ویرایش سطح دسترسی {{$user->name .' '.$user->last_name}}
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST"
                  action="{{route('users.update',['user'=>$user->id])}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label">سطح دسترسی کاربر :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="level" value="{{$user->level}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="text-center">
                    <button class="btn btn-round btn-primary">ویرایش</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>

        CKEDITOR.replace('body', {
            filebrowserUploadUrl: '/admin/upload-image',
            filebrowserImageUploadUrl: '/admin/upload-image',
        });
    </script>
@endsection

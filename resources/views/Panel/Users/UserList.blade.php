@extends('layouts.panel-master')
@section('title','لیست کاربران')
@section('content')
    <!-- page start-->
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست کاربران
                <div class="btn-group pull-left">
                    <a href="{{route('roles.index')}}" class=" btn-sm btn-info">تنظیم سمت</a>
                    <a href="{{route('permissions.index')}}" class="   btn-sm btn-warning">تنظیم دسترسی</a>
                </div>
            </header>
            <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-list"></i></th>
                        <th>تصویر</th>
                        <th>نام و نام خانوادگی</th>
                        <th>ایمیل</th>
                        <th><i class="icon-eye-open"></i> سایر اطلاعات</th>
                        <th><i class="icon-comments"></i> نظرات</th>
                        <th>تاریخ ایجاد</th>
                        <th>تاریخ بروزرسانی</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($users as $user)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="#" class="task-thumb img-rounded">
                                    <img src="" alt="{{$user->name .' '. $user->last_name}}">
                                </a>
                            </td>
                            <td>
                                <a href="{{route('users.profile',['user'=>$user->id])}}">{{$user->name .' '. $user->last_name}}
                                    | {{$user->username}}</a>
                            </td>
                            <td>{{$user->email}}</td>
                            <td>
                                <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-info"><i
                                            class="icon-plus"></i></a>
                            </td>

                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
                                 id="myModal" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                ×
                                            </button>
                                            <h4 class="modal-title">شماره موبایل : {{$user->mobile}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5>درباره کاربر</h5>
                                                    <p>{{$user->about}}</p>
                                                    <h5>سمت کاربر</h5>
                                                    <ul>
                                                        @foreach($user->role as $role)
                                                            <li>{{$role}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <td>
                                {{--<span class="label {{$user->visibility=='show'?'label-success':'label-danger'}}">
                                    <i class="{{$user->visibility=='show'?'icon-eye-open':'icon-eye-close'}}"></i>
                                </span>--}}
                                <span class="label label-inverse"> 8 نظر تایید شده </span>
                            </td>
                            <td>{{jdate($user->created_at)->format('%A, %d %B %y')}}</td>
                            <td class="hidden-phone">{{jdate($user->updated_at)->format('%A, %d %B %y')}}</td>
                            <td>
                                <a href="{{route('users.edit',['user'=>$user->id])}}"
                                   class="btn btn-primary btn-xs"><i
                                            class="icon-pencil"></i></a>
                                <form action="{{route('users.destroy',['user'=>$user->id])}}" method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$users->links()}}
        </section>
    </div>
    <!-- page end-->

    <!-- js placed at the end of the document so the pages load faster -->

    <!--script for this page only-->
    <script src="/Panel/js/dynamic-table.js"></script>
@endsection
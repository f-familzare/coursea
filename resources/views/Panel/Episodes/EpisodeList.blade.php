@extends('layouts.panel-master')
@section('title','لیست محصولات')
@section('content')
    <!-- page start-->
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست ویدئو ها
                <a href="{{route('articles.create')}}" class="pull-left btn-sm btn-default">ویدئو جدید</a>
            </header>
            <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-list"></i></th>
                        <th>عنوان ویدئو/ قسمت</th>
                        <th>دوره</th>
                        <th>نوع / زمان</th>
                        <th><i class="icon-eye-open"></i>بازدید</th>
                        <th><i class="icon-download"></i> تعداد دانلود</th>
                        <th class="hidden-phone"><i class="icon-file-text"></i> نظرات</th>
                        <th><i class="icon-facetime-video"></i> ویدئو</th>
                        <th>تاریخ ایجاد</th>
                        <th>تاریخ بروزرسانی</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($episodes as $episode)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="{{ $episode->path()}}">({{$episode->number}}) {{$episode->title}} </a>
                            </td>
                            <td><a href="#">{{$episode->course->title}}</a></td>
                            <td>{{$episode->type .' / ' .$episode->time}}</td>
                            <td>{{$episode->viewCount}}</td>
                            <td>{{$episode->downloadCount}}</td>
                            <td>
                                <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-warning"><i
                                        class="icon-file-text-alt"></i></a>
                            </td>

                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
                                 id="myModal" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                ×
                                            </button>
                                            <h4 class="modal-title"> {{$episode->title}} ({{$episode->commentCount}}
                                                دیدگاه) </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="card">
                                                    <div class="card-body">
                                                        {!! $episode->description !!}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <td>
                                <a class="btn btn-info btn-xs" href="{{$episode->fileUrl}}"><i
                                        class="icon-download"></i>
                                </a>
                            </td>
                            <td>{{jdate($episode->created_at)->format('%A, %d %B %y')}}</td>
                            <td class="hidden-phone">{{jdate($episode->updated_at)->format('%A, %d %B %y')}}</td>
                            <td>
                                <a href="{{route('episodes.edit',['episode'=>$episode->id])}}"
                                   class="btn btn-primary btn-xs"><i
                                        class="icon-pencil"></i></a>
                                <form action="{{route('episodes.destroy',['episode'=>$episode->id])}}" method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$episodes->links()}}
        </section>
    </div>
    <!-- page end-->

    <!-- js placed at the end of the document so the pages load faster -->

    <!--script for this page only-->
    <script src="/Panel/js/dynamic-table.js"></script>
@endsection

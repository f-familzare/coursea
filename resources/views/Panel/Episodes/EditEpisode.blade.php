@extends('layouts.panel-master')
@section('title','ویرایش ویدئو')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            افزودن ویدئو جدید
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST" action="{{route('episodes.update',['episode'=>$episode->id])}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label">عنوان ویدئو :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="title" value="{{$episode->title}}">
                    </div>

                    <label class="col-sm-2 control-label">برچسب های ویدئو :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="tag" value="{{$episode->tag}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">لینک ویدئو :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="fileUrl" value="{{$episode->fileUrl}}">
                    </div>

                    <label class="col-sm-2 control-label">مدت زمان ویدئو :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="time" value="{{$episode->time}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"> دوره :</label>
                    <div class="col-sm-4">
                        <select name="course_id" class="form-control round-input">
                            <option value="0" disabled selected>انتخاب کنید ...</option>
                            @foreach($courses as $course)
                                <option value="{{$course->id}}" {{$episode->course_id==$course->id?'selected':''}}>{{$course->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label class="col-sm-2 control-label"> شماره قسمت :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="number" value="{{$episode->number}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">نوع ویدئو :</label>
                    <div class="col-sm-4">
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" class="btn btn-success" name="type" id="type1"
                                       value="free"
                                       {{$episode->type=='free'?'checked':''}}>
                                <button type="button" class="btn btn-success btn-sm"> رایگان </button>
                            </label>
                        </div>

                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" name="type" id="type2" value="cash" {{$episode->type=='cash'?'checked':''}}>
                                <button type="button" class="btn btn-info btn-sm"> نقدی </button>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2">توضیحات : </label>
                </div>
                <div class="form-group">
                    <textarea name="description" id="description" cols="30" rows="10"
                              class="form-control col-sm-10">{{$episode->description}}</textarea>
                    <label class="col-sm-2 control-label">وضعیت :</label>
                    <div class="col-sm-4">
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" class="btn btn-success" name="visibility" id="visibility1"
                                       value="show"
                                       checked>
                                <button type="button" class="btn btn-success btn-xs"><i class="icon-eye-open"></i>
                                </button>
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" name="visibility" id="visibility2" value="hidden">
                                <button type="button" class="btn btn-danger btn-xs"><i class="icon-eye-close"></i>
                                </button>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn btn-round btn-primary">ویرایش</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>

        CKEDITOR.replace('description', {
            filebrowserUploadUrl: '/admin/upload-image',
            filebrowserImageUploadUrl: '/admin/upload-image',
        });
    </script>
@endsection


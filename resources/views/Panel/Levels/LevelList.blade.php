@extends('layouts.panel-master')
@section('title','لیست مدیران')
@section('content')
    <!-- page start-->
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست مدیران ها
                <a href="{{route('levels.create')}}" class="pull-left btn-sm btn-default">مدیر جدید</a>
            </header>
            <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-list"></i></th>
                        <th>تصویر</th>
                        <th>نام و نام خانوادگی</th>
                        <th>ایمیل</th>
                        <th><i class="icon-eye-open"></i> سایر اطلاعات</th>
                        <th><i class="icon-comments"></i> نظرات</th>
                        <th>تاریخ ایجاد</th>
                        <th>تاریخ بروزرسانی</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($admins as $admin)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="#" class="task-thumb img-rounded">
                                    <img src="" alt="{{$admin->name .' '. $admin->last_name}}">
                                </a>
                            </td>
                            <td>
                                <a href="{{route('users.profile',['user'=>$admin->id])}}">{{$admin->name .' '. $admin->last_name}}
                                    | {{$admin->username}}</a>
                            </td>
                            <td>{{$admin->email}}</td>
                            <td>
                                <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-info"><i
                                            class="icon-plus"></i></a>
                            </td>

                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
                                 id="myModal" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                ×
                                            </button>
                                            <h4 class="modal-title">شماره موبایل : {{$admin->mobile}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5>درباره کاربر</h5>
                                                    <p>{{$admin->about}}</p>
                                                    <h5>سمت کاربر</h5>
                                                    <ol>
                                                        @foreach($admin->role as $role)
                                                            <li>
                                                                <b>{{$role->role_name}} |</b>
                                                                @foreach($role->permission as $permission)
                                                                     {{$permission->label}} -
                                                                @endforeach
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <td>
                                {{--<span class="label {{$admin->visibility=='show'?'label-success':'label-danger'}}">
                                    <i class="{{$admin->visibility=='show'?'icon-eye-open':'icon-eye-close'}}"></i>
                                </span>--}}
                                <span class="label label-inverse"> 8 نظر تایید شده </span>
                            </td>
                            <td>{{jdate($admin->created_at)->format('%A, %d %B %y')}}</td>
                            <td class="hidden-phone">{{jdate($admin->updated_at)->format('%A, %d %B %y')}}</td>
                            <td>
                                <a href="{{route('levels.edit',['user'=>$admin->id])}}"
                                   class="btn btn-primary btn-xs"><i
                                            class="icon-pencil"></i></a>
                                <form action="{{route('levels.destroy',['user'=>$admin->id])}}" method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$admins->links()}}
        </section>
    </div>
    <!-- page end-->

    <!-- js placed at the end of the document so the pages load faster -->

    <!--script for this page only-->
    <script src="/Panel/js/dynamic-table.js"></script>
@endsection
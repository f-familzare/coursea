@extends('layouts.panel-master')
@section('title','ویرایش مدیر')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            ویرایش سمت های مدیر-  {{$user->name .' '.$user->last_name}} ({{$user->email}})
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST" action="{{route('levels.update',['user'=>$user->id])}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label"> سمت ها : </label>
                    <div class="col-sm-4">
                        <select name="roles" class="form-control round-input roles">
                            <option value="0" disabled>انتخاب کنید ...</option>
                            @foreach($roles as $role)
                                <option value="{{$role->id}}" {{$user->hasRole($role->role_name)?'selected':''}}>{{$role->role_name}} | {{$role->label}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn btn-round btn-primary">ویرایش</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="/panel/js/bootstrap-select.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.roles').selectpicker();
        })
    </script>
@endsection


@extends('layouts.panel-master')
@section('title','افزودن سمت')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            افزودن مدیر جدید
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST" action="{{route('levels.store')}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label"> سمت :</label>
                    <div class="col-sm-4">
                        <select name="roles" class="form-control round-input roles">
                            <option value="0" disabled>انتخاب کنید ...</option>
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->role_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <label class="col-sm-2 control-label"> کاربر :</label>
                    <div class="col-sm-4">
                        <select name="users" class="form-control round-input users" data-live-search="true">
                            <option value="0" disabled>انتخاب کنید ...</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->email}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="text-center">
                    <button class="btn btn-round btn-primary">ذخیره</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="/panel/js/bootstrap-select.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.users').selectpicker();
            $('.roles').selectpicker();
        })
    </script>
@endsection


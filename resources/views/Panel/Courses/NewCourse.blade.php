@extends('layouts.panel-master')
@section('title','افزودن دوره')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            افزودن دوره جدید
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="POST" action="{{route('courses.store')}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                @include('errors.errors')
                <div class="form-group">
                    <label class="col-sm-2 control-label">عنوان دوره :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="title" value="{{old('title')}}">
                    </div>

                    <label class="col-sm-2 control-label">برچسب های دوره :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="tag" value="{{old('tag')}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">تصویر دوره :</label>
                    <div class="col-sm-4">
                        <input type="file" class="form-control round-input" name="imgUrl">
                    </div>

                    <label class="col-sm-2 control-label">مدت زمان دوره :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="time" value="{{old('time')}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"> دسته بندی :</label>
                    <div class="col-sm-4">
                        <select name="category_id" class="form-control round-input">
                            <option value="0" disabled>انتخاب کنید ...</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label class="col-sm-2 control-label"> قیمت :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control round-input" name="price" value="{{old('price')}}">
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="form-group">
                <label class="col-sm-2 control-label">نوع دوره :</label>
                    <div class="col-sm-4">
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" class="btn btn-success" name="type" id="type1"
                                       value="free"
                                       checked>
                                <button type="button" class="btn btn-success btn-sm"> رایگان </button>
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" name="type" id="type2" value="vip">
                                <button type="button" class="btn btn-warning btn-sm"> اعضای ویژه </button>
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" name="type" id="type3" value="cash">
                                <button type="button" class="btn btn-info btn-sm"> نقدی </button>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2">توضیحات : </label>
                </div>
                <div class="form-group">
                    <textarea name="body" id="body" cols="30" rows="10"
                              class="form-control col-sm-10">{{old('body')}}</textarea>
                    <label class="col-sm-2 control-label">وضعیت :</label>
                    <div class="col-sm-4">
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" class="btn btn-success" name="visibility" id="visibility1"
                                       value="show"
                                       checked>
                                <button type="button" class="btn btn-success btn-xs"><i class="icon-eye-open"></i>
                                </button>
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <label>
                                <input type="radio" name="visibility" id="visibility2" value="hidden">
                                <button type="button" class="btn btn-danger btn-xs"><i class="icon-eye-close"></i>
                                </button>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn btn-round btn-primary">ذخیره</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>

        CKEDITOR.replace('body', {
            filebrowserUploadUrl: '/admin/upload-image',
            filebrowserImageUploadUrl: '/admin/upload-image',
        });
    </script>
@endsection


@extends('layouts.panel-master')
@section('title','لیست محصولات')
@section('content')
    <!-- page start-->
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست دوره ها
                <a href="{{route('courses.create')}}" class="pull-left btn-sm btn-default">دوره جدید</a>
            </header>
            <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-list"></i></th>
                        <th>عنوان دوره</th>
                        <th>قیمت</th>
                        <th>مدرس</th>
                        <th><i class="icon-eye-open"></i> بازدید</th>
                        <th><i class="icon-facetime-video"></i> تعداد قسمت </th>
                        <th class="hidden-phone"><i class="icon-file-text"></i> معرفی دوره </th>
                        <th>تاریخ ایجاد</th>
                        <th>تاریخ بروزرسانی</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($courses as $course)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="{{ $course->path()}}">{{$course->title}}</a>
                            </td>
                            <td>{{number_format($course->price)}} تومان </td>
                            <td><a href="#">{{$course->user->name.' '.$course->user->name}}</a></td>
                            <td>{{$course->viewCount}}</td>
                            <td>{{$course->episodeCount}}</td>
                            <td>
                                <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-warning"><i class="icon-file-text-alt"></i></a>
                            </td>

                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                            <h4 class="modal-title">{{$course->title}} (  زمان : {{$course->time}} دقیقه ) </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="card">
                                                    <div class="card-body">
                                                        {!! $course->body !!}</div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <td>{{jdate($course->created_at)->format('%A, %d %B %y')}}</td>
                            <td class="hidden-phone">{{jdate($course->updated_at)->format('%A, %d %B %y')}}</td>
                            <td>
                                <a href="{{route('courses.edit',['course'=>$course->id])}}"
                                   class="btn btn-primary btn-xs"><i
                                            class="icon-pencil"></i></a>
                                <form action="{{route('courses.destroy',['course'=>$course->id])}}" method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$courses->links()}}
        </section>
    </div>
    <!-- page end-->

    <!-- js placed at the end of the document so the pages load faster -->

    <!--script for this page only-->
    <script src="/Panel/js/dynamic-table.js"></script>
@endsection

@extends('layouts.panel-master')
@section('title','لیست محصولات')
@section('content')
    <!-- page start-->
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست نظرات تایید شده
            </header>
            <div class="table-responsive">
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-list"></i></th>
                        <th>فرستنده</th>
                        <th>عنوان مقاله</th>
                        <th class="hidden-phone"><i class="icon-file-text"></i> متن کوتاه</th>
                        <th><i class="icon-stop"></i> وضعیت</th>
                        <th>تاریخ ایجاد</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($approvedComments as $approvedComment)
                        <tr>
                            <td>{{$i++}}</td>
                            <td><a href="#">{{$approvedComment->user->name.' '.($approvedComment->user->last_name!=null?$approvedComment->user->last_name:'')}}</a></td>
                            <td>
                                <a href="{{ $approvedComment->commentable->path()}}">{{$approvedComment->commentable->title}}</a>
                            </td>
                            <td>
                                <a href="#myModal-{{$approvedComment->id}}" data-toggle="modal"
                                   class="btn btn-xs btn-warning"><i
                                        class="icon-file-text-alt"></i></a>
                            </td>

                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
                                 id="myModal-{{$approvedComment->id}}" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                ×
                                            </button>
                                            <h4 class="modal-title">متن پیام</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="card">
                                                    <div class="card-body">
                                                        {!!$approvedComment->comment!!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <td>
                                <span
                                    class="label {{$approvedComment->visibility=='show'?'label-success':'label-danger'}}">
                                    <i class="{{$approvedComment->visibility=='show'?'icon-eye-open':'icon-eye-close'}}"></i>
                                </span>
                            </td>
                            <td>{{jdate($approvedComment->created_at)->format('%A, %d %B %y')}}</td>
                            <td>
                                <form action="{{route('comments.reject',['comment'=>$approvedComment->id])}}"
                                      method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('PUT')}}
                                    <button class="btn btn-warning btn-xs">
                                        <i class="icon-minus"></i>
                                    </button>
                                </form>
                                <form action="{{route('comments.destroy',['comment'=>$approvedComment->id])}}"
                                      method="POST"
                                      style="display: inline">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-xs"><i class="icon-trash "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{--            {{$approvedComments->links()}}--}}
        </section>
    </div>
    <!-- page end-->

    <!-- js placed at the end of the document so the pages load faster -->

    <!--script for this page only-->
    <script src="/Panel/js/dynamic-table.js"></script>
@endsection


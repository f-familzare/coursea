@extends('layouts.master')
{{--@section('title',' وبسایت Coursea')--}}
@section('content')

    <!-- Jumbotron Header -->
    <header class="jumbotron hero-spacer">
        <h1>{{__('site.home.banner.welcome')}}</h1>
        <p>{{__('site.home.banner.description')}}</p>
    </header>

    <hr>
    @if($courses)
        <div class="row">
            <div class="col-lg-12">
                <h3>{{__('site.search.course results')}}</h3>
            </div>
        </div>

        <div class="row ">
            @foreach($courses as $course)
                <div class="col-md-3 col-sm-6 hero-feature">
                    <div class="thumbnail">
                        <img src="{{$course->imgUrl['thumb']}}" alt="{{$course->title}}">
                        <div class="caption">
                            <h3><a href="/{{$course->path()}}">{{$course->title}}</a></h3>
                            <p>
                                {{Str::limit($course->description,100)}}
                            </p>
                            <p>
                                <a href="{{$course->path()}}"
                                   class="btn btn-primary">{{__('site.home.buttons.view')}}</a>
                                <a href="#"
                                   class="btn btn-default">{{__('site.home.buttons.add to cart')}}</a>
                            </p>
                        </div>
                        <div class="ratings">
                            {{--View Count Using Redis--}}
                            {{--                        <p class="pull-left">{{Redis::get("views.{$course->id}.courses")!=null?Redis::get("views.{$course->id}.courses"):0}}{{__('site.home.buttons.view')}}</p>--}}
                            {{--View Count Using icriment method--}}
                            <p class="pull-left">{{$course->viewCount!=null?$course->viewCount:0}} {{__('site.home.buttons.view')}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
        <hr>
    @if($articles)

        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-12">
                    <h3>{{__('site.search.article results')}}</h3>
                </div>
                @foreach($articles as $article)
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="{{$article->imgsUrl['thumb']}}" alt="">
                            <div class="caption">
                                <h4><a href="/{{$article->path()}}">{{$article->title}}</a>
                                </h4>
                                <p>{{Str::limit($article->description,100)}}</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-left">
                                {{--View Count Using Redis--}}
                                {{--{{Redis::get("views.{$article->id}.articles")!=null?Redis::get("views.{$article->id}.articles"):0}} {{__('site.home.buttons.view')}}</p>--}}

                                {{--View Count Using icriment method--}}
                                <p class="pull-left">{{$article->viewCount!=null?$article->viewCount:0}} {{__('site.home.buttons.view')}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    @empty([$courses ,$articles])
        <h3>نتیجه ای یافت نشد</h3>
    @endempty



@endsection

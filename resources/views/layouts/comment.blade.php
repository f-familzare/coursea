@if(auth()->check())
    <!-- Comments Form -->
    <div class="well">
        <h4>ثبت نظر :</h4>
        <form role="form" action="{{route('comment')}}" method="POST">
            {{csrf_field()}}
            <input type="hidden" name="commentable_id" value="{{$subject->id}}">
            <input type="hidden" name="commentable_type" value="{{get_class($subject)}}">
            <input type="hidden" name="parent_id" value="0">
            <div class="form-group">
                <textarea class="form-control" rows="3" name="comment"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">ارسال</button>
        </form>
    </div>

    <hr>

    <!-- Posted Comments -->

    <!-- Comment -->
    @foreach($comments as $comment)
        <div class="media">
            <a class="pull-right" href="#">
                <img class="media-object" src="http://placehold.it/64x64" alt="">
            </a>
            <div class="media-body">
                <h4 class="media-heading">{{$comment->user->name.' '. ($comment->user->last_name!= null ?$comment->user->last_name:'')}}
                    <small>{{jdate($comment->created_at)->ago()}}</small>
                    <button class="pull-left btn btn-xs btn-success" data-toggle="modal" data-target="#sendCommentModal"
                            data-parent="{{$comment->id}}">پاسخ
                    </button>
                </h4>
                {!! $comment->comment !!}
                @if(count($comment->childComment))
                    @foreach($comment->childComment as $childComment)
                    <!-- Nested Comment -->
                        <div class="media">
                            <a class="pull-right" href="#">
                                <img class="media-object" src="http://placehold.it/64x64" alt="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">{{$childComment->user->name.' '.($childComment->user->last_name!=null?$childComment->user->last_name:'')}}
                                    <small>{{jdate($childComment->created_at)->ago()}}</small>
                                </h4>
                                {!! $childComment->comment !!}
                            </div>
                        </div>
                        <!-- End Nested Comment -->
                    @endforeach
                @endif
            </div>
        </div>
    @endforeach

    <div class="modal fade" id="sendCommentModal" tabindex="-1" role="dialog" aria-labelledby="sendCommentModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"></span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">ارسال پاسخ</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('comment')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="commentable_id" value="{{$subject->id}}">
                        <input type="hidden" name="commentable_type" value="{{get_class($subject)}}">
                        <input type="hidden" name='parent_id' value="0">
                        <div class="form-group">
                            <label for="message-text" class="control-label">متن پاسخ:</label>
                            <textarea class="form-control" id="message-text" name="comment"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">ارسال</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@else
    <hr>
    <h4>برای ارسال نظر وارد شوید ... </h4>
    <a href="{{route('login')}}" class="btn btn-success">ورود</a>

@endif
@section('script')
    <script>
        $('#sendCommentModal').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let parentId = button.data('parent');
            let modal = $(this);
            modal.find("[name='parent_id']").val(parentId);
        });
    </script>
@endsection

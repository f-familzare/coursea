<!DOCTYPE html>
<html lang="fa">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--    <meta name="description" content="">--}}
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEOMeta::generate() !!}
{{--    <title>@yield('title')</title>--}}
    <link rel="alternate" title="فید های کورس آ" type="application\rss+xml" href="{{route('feed.article')}}">
    <!-- Bootstrap Core CSS -->
    <link href="/css/home.css" rel="stylesheet">
    <script src="/js/SweetAlert.min.js"></script>
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{__('site.home.brand')}}</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <a href="#">{{__('site.home.menu.courses')}}</a>
                </li>
                <li>
                    <a href="#">{{__('site.home.menu.articles')}}</a>
                </li>
                <li>
                    <a href="#">{{__('site.home.menu.about us')}}</a>
                </li>

                @if(auth()->check())
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="icon-key"></i> {{__('site.home.menu.logout')}}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </li>
                    <li>
                        <a href="{{auth()->user()->level =='admin'?route('panel'):route('user.panel.index')}}" target="_blank"><i class="fa fa-user"></i></a>
                    </li>
                @else
                   <li>
                       <a class="dropdown-item" href="{{ route('login') }}">
                           <i class="icon-key"></i> {{__('site.home.menu.login')}}
                       </a>
                   </li>
                    <li>
                       <a class="dropdown-item" href="{{ route('register') }}">
                           <i class="icon-key"></i> {{__('site.home.menu.register')}}
                       </a>
                   </li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div id="app" class="container">

    <div class="row">
        @yield('content')
    </div>

</div>
<!-- /.container -->

<div class="container">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Your Website 2014</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->


<script src="/js/app.js"></script>
@include('sweet::alert')
@yield('script')
</body>

</html>

<?php
return [
    'home' => [
        'brand' => 'Coursea Website',
        'menu' => [
            'courses' => 'Courses',
            'articles' => 'Articles',
            'about us' => 'About Us',
            'login' => 'Login',
            'register' => 'Register',
            'logout' => 'LogOut',
        ],
        'banner' => [
            'welcome' => 'Welcome To Coursea',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.',

        ],
        'courses' => [
            'titer' => 'Latest Courses'
        ],
        'articles' => [
            'titer' => 'Latest Articles'
        ],
        'buttons' => [
            'add to cart' => 'Add To Cart',
            'more info' => 'More Info',
            'view' => 'View'
        ],


    ],
    'search' => [
        'course results' => 'Courses Search Results',
        'article results' => 'Articles Search Results',
    ]
];

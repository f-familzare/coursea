<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Model\Article;
use App\Model\Category;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;

class ReadArticleTest extends TestCase
{

    use  RefreshDatabase,DatabaseMigrations;

    protected $user, $article, $category;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create([
            'verified' => 1
        ]);
        $this->category = factory(Category::class)->create();
        $this->article = factory(Article::class)->create([
            'user_id' => $this->category->id,
            'category_id'=>$this->user->id
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @test
     */
    public function readArticle()
    {

        $response = $this->get($this->article->path());

        $response->assertSee($this->article->title);
    }
}

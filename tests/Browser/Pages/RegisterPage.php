<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page;

class RegisterPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/fa/register';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param Browser $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
        ->assertSee('ثبت نام');
    }

    public function register(Browser $browser,$name,$last_name,$email,$password)
    {
        $browser
            ->type('name', $name)
            ->type('last_name', $last_name)
            ->type('email', $email)
            ->type('password', $password)
            ->type('password_confirmation', $password)
            ->press('ثبت نام');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name' => 'input[name=name]',
            '@last_name' => 'input[name=last_name]',
            '@email' => 'input[name=email]',
            '@password' => 'input[name=password]',
            '@password_confirmation' => 'input[name=password]',

        ];
    }
}

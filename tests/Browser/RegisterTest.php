<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\RegisterPage;
use Tests\DuskTestCase;

class RegisterTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @test
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new RegisterPage)
                ->register('admin','adminn','email@email.com',123456);
        });
    }
}

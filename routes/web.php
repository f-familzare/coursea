<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  Site Routes
Route::get('/', 'HomeController@index')->name('home');
Route::post('/comment', 'HomeController@comment')->name('comment');
// Search Route
Route::get('/search', 'HomeController@search')->name('search');


Route::namespace('Site')->group(function () {
    Route::get('/articles', 'ArticleController@index')->name('articles');
    Route::get('/article/{articleSlug}', 'ArticleController@single')->name('articles.show');
    Route::get('/courses', 'CourseController@index')->name('courses');
    Route::get('/course/{courseSlug}', 'CourseController@single')->name('courses.show');
    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/categories/{categoryName}', 'CategoryController@single')->name('categories.show');
    Route::get('/active/email/{token}', 'ActivationCodeController@active')->name('active.email');


    // Download Route
    Route::get('/download/{episode}', 'CourseController@download');

    //Sitemap Routes
    Route::prefix('sitemap')->group(function () {
        Route::get('/', 'SitemapController@index');
        Route::get('/course', 'SitemapController@course');
        Route::get('/article', 'SitemapController@article');
        Route::get('/episode', 'SitemapController@episode');
    });
    //Feed - RSS Routes
    Route::prefix('feed')->group(function () {
        Route::get('/course', 'SitemapController@course');
        Route::get('/article', 'SitemapController@article')->name('feed.article');
        Route::get('/episode', 'SitemapController@episode');
    });

    //Payment Routes
    Route::post('/course/payment', 'PaymentController@payment')->name('course.payment');
    Route::get('/course/payment/checker', 'PaymentController@checker')->name('course.payment.checker');

    Route::get('/active', 'ActivationCodeController@jobs');
});

//User Routes
Route::namespace('UserPanel')->prefix('user')->middleware('auth')->group(function () {
    Route::get('/', 'UserPanelController@index')->name('user.panel.index');
    Route::get('/history', 'UserPanelController@history')->name('user.panel.history');
    Route::get('/vip', 'UserPanelController@vip')->name('user.panel.vip');

    Route::post('/payment', 'UserPanelController@payment')->name('user.panel.payment');
    Route::get('/payment/checker', 'UserPanelController@checker')->name('user.panel.checker');

});


//  Panel Routes
Route::namespace('Panel')->middleware('CheckAdmin')->prefix('admin')->group(function () {
    Route::get('/', 'PanelController@index')->name('panel');
    Route::post('/upload-image', 'PanelController@ckImageUploader');
    Route::resource('/articles', 'ArticleController');
    Route::resource('/categories', 'CategoryController');
    Route::resource('/courses', 'CourseController');
    Route::resource('/episodes', 'EpisodeController');
    Route::resource('/roles', 'RoleController');
    Route::resource('/permissions', 'PermissionController');
    Route::resource('/levels', 'LevelController')->parameters(['levels' => 'user']);
    Route::resource('/comments', 'CommentController')->except(['index', 'create', 'store', 'show']);
    Route::get('/comments/approved', 'CommentController@approved')->name('comments.approved');
    Route::get('/comments/unapproved', 'CommentController@unapproved')->name('comments.unapproved');
    Route::any('/comments/reject/{comment}', 'CommentController@reject')->name('comments.reject');


    Route::prefix('users')->group(function () {
        Route::get('/', 'UserController@index')->name('users.index');
        Route::get('/profile/{user}', 'UserController@profile')->name('users.profile');
        Route::get('/edit/{user}', 'UserController@edit')->name('users.edit');
        Route::put('/update/{user}', 'UserController@update')->name('users.update');
        Route::delete('/destroy/{user}', 'UserController@destroy')->name('users.destroy');
    });
});

//Auth::routes();
Route::namespace('Auth')->group(function () {

    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');

    //  Socialite Package --  Github
    Route::get('login/github', 'LoginController@redirectToProviderGithub')->name('login.github');
    Route::get('login/github/callback', 'LoginController@handleProviderCallbackGithub');

    //  Socialite Package --  Google
    Route::get('login/google', 'LoginController@redirectToProvider')->name('login.google');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');
});



//Route::get('/home', 'HomeController@index');

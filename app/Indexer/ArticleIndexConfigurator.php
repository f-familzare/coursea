<?php


namespace App\Indexer;


use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class ArticleIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    /**
     * @var array
     */
    protected $settings = [
        //
    ];
}

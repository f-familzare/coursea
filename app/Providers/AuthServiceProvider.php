<?php

namespace App\Providers;

use App\Model\Permission;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        foreach ($this->getPermission() as $permission) {
            Gate::define($permission->permission_name, function ($user) use ($permission) {
                return $user->hasRole($permission->role);
            });
        }


        Passport::routes();
    }

    public function getPermission()
    {
        return Permission::with('role')->get();
    }
}

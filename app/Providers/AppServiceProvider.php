<?php

namespace App\Providers;

use App\Model\Comment;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\Console\DuskCommand;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Validator::extend('recaptcha', function ($attribute, $value, $parameters, $validator) {
            $client = new Client();
            $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => [
                    'secret' => config('services.recaptcha.secret_key'),
                    'response' => $value,
                    'remoteip' => request()->ip(),
//                    'nested_field' => [
//                        'nested' => 'hello'
//                    ]
                ]
            ]);
            $response = json_decode($response->getBody());

            return $response->success;
        });

        View::composer('layouts.panel-master', function ($view) {
            $accepted = Comment::whereVisibility('show')->count();
            $view->with(['accepted'=>$accepted]);
        });

        View::composer('layouts.panel-master', function ($view) {
            $reject = Comment::whereVisibility('hidden')->count();
            $view->with(['reject'=>$reject]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        Gate::define('Add_article', function ($user) {
//        });
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
            $this->commands(DuskCommand::class);
        }
    }


}


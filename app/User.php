<?php

namespace App;

use App\Model\ActivationCode;
use App\Model\Article;
use App\Model\Comment;
use App\Model\Course;
use App\Model\HasRole;
use App\Model\Learn;
use App\Model\Payment;
use App\Model\Role;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasRole, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_name', 'avatar', 'username', 'mobile', 'about', 'level', 'verified', 'api_token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function article()
    {
        return $this->hasMany(Article::class);
    }

    public function course()
    {
        return $this->hasMany(Course::class);
    }

    public function isAdmin()
    {
        return $this->level == 'admin' ? true : false;
    }

    public function activationCode()
    {
        return $this->hasMany(ActivationCode::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function payment()
    {
        return $this->hasMany(Payment::class);
    }

    public function isVip()
    {
        return $this->viptime > Carbon::now() ? true : false;
    }

    public function learn()
    {
        return $this->hasMany(Learn::class);
    }

    public function isLearning($course)
    {
        $learn = Learn::where('user_id', $this->id)->where('course_id', $course->id)->first();
        return $learn ? true : false;
    }
}

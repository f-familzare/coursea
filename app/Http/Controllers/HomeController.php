<?php

namespace App\Http\Controllers;

use App\Model\Article;
use App\Model\Course;
use SEO;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $locale = app()->getLocale();
        \cache()->flush();
        \cache()->pull('articles');
        \cache()->pull('courses');
        SEO::setTitle('وب سایت آموزشی Coursea');
        SEO::setDescription('وب سایتی برای آموزش و قرار دادن دوزه های مختلف');
        if (cache()->has('articles')) {
            $articles = cache('articles');
        } else {
            $articles = Article::whereLang($locale)->latest()->limit(3)->get();
            cache()->put('articles', $articles, Carbon::now()->addMinutes(15));

        }

        if (cache()->has('courses')) {
            $courses = cache('courses');
        } else {
            $courses = Course::whereLang($locale)->latest()->limit(3)->get();
            cache()->put('courses', $courses, Carbon::now()->addMinutes(15));
        }

        return view('/Site.index', compact('articles', 'courses'));
    }


    public function comment(Request $request)
    {
        auth()->user()->comment()->create($request->all());
        return redirect()->back();
    }

    public function search()
    {
        $q = request('search');
        $articles= Article::search($q)->get();
        $courses= Course::search($q)->get();
        return view('Site.search',compact('articles','courses'));
    }
}

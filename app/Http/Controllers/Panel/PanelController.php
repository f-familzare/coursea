<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Model\Permission;
use App\Model\Role;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use Morilog\Jalali\Jalalian;

class PanelController extends Controller
{
    public function index()
    {
        return view('Panel.index');
    }


    /**
     * For Upload panel's image
     * @param $file - $request file
     * @param $part - directory
     * @return mixed
     */
    protected function imgUploader($file, $part)
    {
        if (!is_null($file)) {
            $date = Jalalian::fromDateTime(Carbon::now())->format('y-m');
            $filePath = "/Uploads/{$part}/{$date}/";
            $fileName = $file->getClientOriginalName();
            $file = $file->move(public_path($filePath), $fileName);

            $sizes = ['300', '600', '900'];
            $url['imgsUrl'] = $this->resize($sizes, $file->getRealPath(), $fileName, $filePath);
            $url['thumb'] = $url['imgsUrl'][$sizes[0]];
            return $url;
        }
    }

    /**
     * For resize pictures using intervention package
     * @param $sizes - custom size for resize
     * @param $path - original image path
     * @param $fileName - original image name
     * @param $filePath - resize image path
     * @return mixed
     */
    protected function resize($sizes, $path, $fileName, $filePath)
    {
        $imgsUrl['original'] = $filePath . $fileName;
        foreach ($sizes as $size) {
            $imgsUrl[$size] = $filePath . "{$size}px_" . $fileName;
            Image::make($path)->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path($imgsUrl[$size]));
        }
        return $imgsUrl;
    }

    /**
     * For delete upload image $this->imgUploader()
     * @param $files
     */
    protected function imgDelete($files)
    {
        foreach ($files as $key => $file) {
            unlink(public_path($file));
        }
    }

    public function ckImageUploader()
    {

        $date = Jalalian::fromDateTime(Carbon::now())->format('y-m-d');
        $file = request()->file('upload');
        $fileName = $file->getClientOriginalName();
        $filePath = "/Uploads/CkEditor/{$date}/";
        $url = $file->move(public_path($filePath), $fileName);
        return "<script>window.parent.CKEDITOR.tools.callFunction(1,'{$url}','')</script>";
    }

    protected function setCourseTime($episode)
    {
        $course = $episode->course;
        $course->time = $this->getCourseTime($course->episode->pluck('time'));
        $course->save();
    }

    protected function getCourseTime($times)
    {
        $timestamp = Carbon::parse('00:00:00');
        foreach ($times as $time) {
            $courseTime = strlen($time) == 5 ? strtotime('00:' . $time) : strtotime($time);
            $timestamp->addSecond($courseTime);
        }
        return $timestamp->format('H:i:s');
    }

}
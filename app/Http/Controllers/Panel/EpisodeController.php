<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\EpisodeRequest;
use App\Model\Course;
use App\Model\Episode;
use Illuminate\Http\Request;

class EpisodeController extends PanelController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $episodes=Episode::with('course')->latest()->paginate(20);
        return view('/Panel.Episodes.EpisodeList',compact('episodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses=Course::all();
        return view('/Panel.Episodes.NewEpisode',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EpisodeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EpisodeRequest $request)
    {
        $episode=Episode::create([
            'course_id'=>$request->input('course_id'),
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'type'=>$request->input('type'),
            'tag'=>$request->input('tag'),
            'fileUrl'=>$request->input('fileUrl'),
            'time'=>$request->input('time'),
            'number'=>$request->input('number'),
        ]);
        $this->setCourseTime($episode);
        return redirect(route('episodes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Episode $episode
     * @return void
     */
    public function show(Episode $episode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Episode $episode
     * @return \Illuminate\Http\Response
     */
    public function edit(Episode $episode)
    {
        $courses=Course::all();
        return view('/Panel.Episodes.EditEpisode',compact('episode','courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EpisodeRequest $request
     * @param Episode $episode
     * @return \Illuminate\Http\Response
     */
    public function update(EpisodeRequest $request, Episode $episode)
    {
        $episode->update([
            'course_id'=>$request->input('course_id'),
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'type'=>$request->input('type'),
            'tag'=>$request->input('tag'),
            'fileUrl'=>$request->input('fileUrl'),
            'time'=>$request->input('time'),
            'number'=>$request->input('number'),
        ]);
        $this->setCourseTime($episode);
        return redirect(route('episodes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Episode $episode
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Episode $episode)
    {
        $episode->delete();
        return redirect(route('episodes.index'));
    }
}

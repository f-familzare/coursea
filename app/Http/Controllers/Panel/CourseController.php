<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\CourseRequest;
use App\Model\Category;
use App\Model\Course;
use App\Model\Episode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CourseController extends PanelController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $courses = Course::with('user', 'category')->latest()->paginate(20);
        return view('/Panel.Courses.CourseList', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::where('subject', 'course')->get();
        return view('/Panel.Courses.NewCourse', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CourseRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CourseRequest $request)
    {
        Auth::loginUsingId(1);
        $imagesUrl = $this->imgUploader($request->file('imgUrl'), 'Courses');
        auth()->user()->course()->create(array_merge([
            'title' => $request->input('title'),
            'category_id' => $request->input('category_id'),
            'body' => $request->input('body'),
            'price' => $request->input('price'),
            'type' => $request->input('type'),
            'tag' => $request->input('tag'),
            'time' => $request->input('time'),
            'imgUrl' => $imagesUrl,
        ]));
        return redirect(route('courses.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Course $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Course $course
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Course $course)
    {
        $categories = Category::where('subject', 'course')->get();
        $course->with('user', 'category')->get();
        return view('/Panel.Courses.EditCourse', compact('course', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CourseRequest $request
     * @param \App\Model\Course $course
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CourseRequest $request, Course $course)
    {
        $files = $course->imgUrl;
        if ($request->hasFile('imgUrl')) {
            if ($files) {
                $this->imgDelete($files['imgsUrl']);
            }
            $imagesUrl = $this->imgUploader($request->file('imgUrl'), 'Courses');
            $course->update(array_merge(['imgUrl' => $imagesUrl]));
        }
        $course->update([
            'title' => $request->input('title'),
            'category_id' => $request->input('category_id'),
            'body' => $request->input('body'),
            'price' => $request->input('price'),
            'type' => $request->input('type'),
            'tag' => $request->input('tag'),
            'time' => $request->input('time'),
        ]);
        return redirect(route('courses.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Course $course
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Course $course)
    {
        $files = $course->imgUrl;
        if ($files) {
            $this->imgDelete($files['imgsUrl']);
        }
        $course->delete();
        return redirect(route('courses.index'));
    }
}

<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\ArticleRequest;
use App\Model\Article;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ArticleController extends PanelController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $articles = Article::with('user')->latest()->paginate(20);
//        return response()->json(['articles'=>$articles , 'status'=>200],200);
        return view('/Panel.Articles.ArticleList', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('/Panel.Articles.NewArticle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleRequest $request
     * @return array
     */
    public function store(ArticleRequest $request)
    {
        Auth::loginUsingId(1);
        $imagesUrl = $this->imgUploader($request->file('imgsUrl'), 'Articles');
        auth()->user()->article()->create(array_merge([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'body' => $request->input('body'),
            'tag' => $request->input('tag'),
            'time' => $request->input('time'),
            'visibility' => $request->input('visibility'),
            'imgsUrl' => $imagesUrl]));
        return redirect(route('articles.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Article $article
     * @return void
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Article $article)
    {
        return view('/Panel.Articles.EditArticle', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArticleRequest $request
     * @param  \App\Model\Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $files=$article->imgsUrl;
        if ($request->hasFile('imgsUrl')) {
            if ($files) {
                $this->imgDelete($files['imgsUrl']);
            }
            $imagesUrl = $this->imgUploader($request->file('imgsUrl'), 'Articles');
            $article->update(array_merge(['imgsUrl' => $imagesUrl]));
        }
        $article->update(array_merge([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'body' => $request->input('body'),
            'tag' => $request->input('tag'),
            'time' => $request->input('time'),
            'visibility' => $request->input('visibility'),
        ]));
        return redirect(route('articles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Article $article)
    {
        $files=$article->imgsUrl;
        if ($files) {
            $this->imgDelete($files['imgsUrl']);
        }
        $article->delete();
        return redirect(route('articles.index'));
    }
}

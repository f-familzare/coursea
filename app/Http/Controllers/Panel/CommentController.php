<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Model\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function approved()
    {
        $approvedComments = Comment::whereVisibility('show')->with('commentable')->get();
        return view('Panel.Comments.CommentList', compact('approvedComments'));
    }

    public function unapproved()
    {
        $unapprovedComments = Comment::where('visibility', 'hidden')->get();
        return view('Panel.Comments.UnapprovedCommentList', compact('unapprovedComments'));
    }

    public function edit(Comment $comment)
    {
        //
    }

    public function update(Request $request,Comment $comment)
    {
        $comment->update(['visibility' => 'show']);
        $comment->commentable->increment('commentCount');
        return redirect()->back();
    }

    public function reject(Request $request, Comment $comment)
    {
        $comment->update(['visibility' => 'hidden']);
        $comment->commentable->decrement('commentCount');
        return redirect()->back();
    }

    public function destroy(Comment $comment)
    {
        $comment->commentable->decrement('commentCount');
        $comment->delete();
        return redirect()->back();
    }
}

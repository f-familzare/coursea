<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
//   install passport composer require laravel/passport "~9.0"
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return response(['data' => $validator->errors()->all(), 'status' => 403], 401);
        } else if (! auth()->validate(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return response(['data' => 'شما ثبت نام نکرده اید', 'status' => 404], 404);
        }
        return response(['data' => User::whereEmail($request->input('email'))->first()->api_token, 'status' => 200],200);
    }
}

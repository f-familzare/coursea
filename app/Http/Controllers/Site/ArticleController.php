<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Model\Article;
use App\Model\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class ArticleController extends Controller
{
    public function single(Article $article)
    {
//        Redis::incr("views.{$article->id}.articles");
        $article->increment('viewCount');
        $comments = $article->comments()->where('parent_id', 0)->where('visibility', 'show')->latest()->with(['user', 'childComment' => function ($response) {
            $response->where('visibility', 'show')->get();
        }])->get();
        return view('Site.article', compact('article', 'comments'));
    }
}

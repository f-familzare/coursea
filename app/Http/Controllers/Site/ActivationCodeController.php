<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Jobs\SendMail;
use App\Model\ActivationCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ActivationCodeController extends Controller
{
    public function active($token)
    {
        $activationCode = ActivationCode::whereCode($token)->first();
        if (!$activationCode) {
            dd('code is invalid');
        }
        if ($activationCode->used == 1) {
            dd('error-register again');
        }
        if ($activationCode->expire < Carbon::now()) {
            dd('expire Code');
        }
        $activationCode->update(['used' => 1]);
        $activationCode->user()->update(['verified' => 1]);
        auth()->loginUsingId($activationCode->user->id);
        return view('welcome');
//        return $token;
    }

    public function jobs()
    {
        dispatch(new SendMail(auth()->user(),Str::random(5)))->onQueue('send:mail')->delay(10);
        return 'done';
    }
}

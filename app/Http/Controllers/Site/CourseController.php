<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Model\Article;
use App\Model\Comment;
use App\Model\Course;
use App\Model\Episode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;

class CourseController extends Controller
{
    public function single(Course $course)
    {
//        Redis::incr("views.{$course->id}.courses");

        $course->with('episode')->get();
        $course->increment("viewCount");
        $comments = $course->comments()->where('parent_id', 0)->where('visibility', 'show')->latest()->with(['user', 'childComment' => function ($response) {
            $response->where('visibility', 'show')->get();
        }])->get();


        return view('Site.courses', compact('course', 'comments'));
    }

    public function download(Episode $episode)
    {
        $hash = 'werty@uioppdfghj#klqwertyu' . $episode->id . \request()->ip() . \request('t');
        if (Hash::check($hash, \request('mac'))) {
            return response()->download(storage_path($episode->fileUrl));
        }

        return 'لینک دانلود تامغتبر است';
    }
}

<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Model\Course;
use App\Model\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $MerchantId = 'f83cc956-f59f-11e6-889a-005056a205be'; //Required

    public function payment()
    {

        $course = Course::findOrFail(\request('course_id'));
        if (auth()->user()->isLearning($course)){
            alert('این دوره قبلا خریداری شده')->error('توجه')->confirmButton('باشه');
        }
        if ($course->price == 0 && $course->type == 'vip') {
            alert('این دوره قابل خریداری نیست')->error('توجه')->confirmButton('باشه');
        }
        $data = [
            "merchant_id" => $this->MerchantId,
            "amount" => $course->price,
            "callback_url" => route('course.payment.checker'),
            "description" => "خرید تست",
            "metadata" => ["email" => auth()->user()->email],
        ];
        $jsonData = json_encode($data);
        $ch = curl_init('https://api.zarinpal.com/pg/v4/payment/request.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));

        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true, JSON_PRETTY_PRINT);
        curl_close($ch);


        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            if (empty($result['errors'])) {
                if ($result['data']['code'] == 100) {

                    auth()->user()->payment()->create([
                        'resnumber' => $result['data']['authority'],
                        'price' => $course->price,
                        'course_id' => $course->id

                    ]);
                    return redirect('https://www.zarinpal.com/pg/StartPay/' . $result['data']["authority"]);
                }
            } else {
                echo 'Error Code: ' . $result['errors']['code'];
                echo 'message: ' . $result['errors']['message'];

            }
        }
    }

    public function checker()
    {
        $Authority = \request('Authority');
        $payment = Payment::whereResnumber($Authority)->firstOrFail;
        $course = Course::findOrFail($payment->course_id);
        $data = [
            "merchant_id" => $this->MerchantId,
            "authority" => $Authority,
            "amount" => $payment->price
        ];
        $jsonData = json_encode($data);
        $ch = curl_init('https://api.zarinpal.com/pg/v4/payment/verify.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v4');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            if ($result['data']['code'] == 100) {
                if ($this->checkPayment($payment, $course)) {
                    alert()->success('عملیات مورد نظر با موفقیت انجام شد', 'با تشکر');
                }
                echo 'Transation success. RefID:' . $result['data']['ref_id'];
            } else {
                echo 'code: ' . $result['errors']['code'];
                echo 'message: ' . $result['errors']['message'];
            }
        }
    }

    protected function checkPayment($payment, $course)
    {
        $payment->update(['status' => 1]);
        auth()->user()->learn()->create(['course_id'=>$course->id]);
        return true;

    }
}

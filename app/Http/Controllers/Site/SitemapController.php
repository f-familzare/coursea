<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Model\Article;
use App\Model\Course;
use App\Model\Episode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SitemapController extends Controller
{
    public function index()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap', 30);
        if (!$sitemap->isCached()) {
            $sitemap->addSitemap(url()->to('/'));
        }
        return $sitemap->render('sitemapindex');
    }

    public function course()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap.course', 60);
        if (!$sitemap->isCached()) {
            $courses = Course::latest()->get();
            // add every post to the sitemap
            foreach ($courses as $course) {
                $sitemap->addSitemap(url()->to($course->path()), $course->created_at, '0.9', 'weekly');
            }
        }
        return $sitemap->render('sitemapindex');
    }

    public function article()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap.article');
        if (!$sitemap->isCached()) {
            $articles = Article::latest()->get();
            foreach ($articles as $article) {
                $sitemap->addSitemap(url()->to($article->path()), $article->created_at, '0.8', 'weekly');
            }
        }
        return $sitemap->render('sitemapindex');
    }

    public function episode()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap.episode');
        if (!$sitemap->isCached()) {
            $episodes = Episode::latest()->get();
            foreach ($episodes as $episode) {
                $sitemap->addSitemap(url()->to($episode->path()), $episode->created_at, '0.9', 'daily');
            }
        }
        $sitemap->render('sitemapindex');
    }
}

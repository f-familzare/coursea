<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Model\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FeedController extends Controller
{
    public function article()
    {
        $feed = App::make("feed");
        // cache the feed for 60 minutes (second parameter is optional)
        $feed->setCache(60, 'laravelFeedKey');
        if (!$feed->isCached()) {
            // creating rss feed with our most recent 20 posts
            $articles = Article::latest()->limit(20)->get();
            {
                foreach ($articles as $article) {
                    // set your feed's title, description, link, pubdate and language
                    $feed->title = $article->title;
                    $feed->description = $article->description;
                    $feed->logo = $article->imgsUrl['thumb'];
                    $feed->link = url($article->path());
                    $feed->pubdate = $article->created_at;
                    $feed->lang = 'fa';
                    $feed->setShortening(true); // true or false
                    $feed->setTextLimit(100); // maximum length of description text
                    // set item's title, author, url, pubdate, description, content, enclosure (optional)*
                    $feed->add($article->title, $article->user->name, url()->to($article->path()), $article->created_at, $article->description, $article->body);
                }
            }

        }
        return $feed->render('atom');
    }

    public function course()
    {

    }

    public function episode()
    {

    }
}

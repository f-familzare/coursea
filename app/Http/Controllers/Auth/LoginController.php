<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use App\Model\ActivationCode;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (auth()->validate($request->only('email', 'password'))) {
            $user = User::whereEmail($request->input('email'))->first();
            if ($user->verified == 0) {
                $codeStatus = $user->activationCode()->where('expire', '>=', Carbon::now())->latest()->first();
                if (count([$codeStatus]) == 1) {
                    if($codeStatus->expire > Carbon::now() ) {
                        $this->incrementLoginAttempts($request);
                        return back()->withErrors(['code' => 'ایمیل فعال سازی قبلا به ایمیل شما ارسال شد بعد از 15 دقیقه دوباره برای ارسال ایمیل لاگین کنید']);
                    } else {
                        event(new UserRegistered($user));
                    }
                }
            }
        }

        if ($this->attemptLogin($request)) {
            alert()->success('', 'با موفقیت وارد شدید')->autoclose(3500)->persistent('تایید');
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleProviderCallback()
    {
        $google_user = Socialite::driver('google')->user();
        if ($google_user) {
            $user = User::whereEmail($google_user->getEmail())->first();
            if (!$user) {
                $user = User::create([
                    'name' => $google_user->getName(),
                    'last_name' => ' ',
                    'avatar' => $google_user->getAvatar(),
                    'username' => Str::random(5),
                    'email' => $google_user->getEmail(),
                    'password' => bcrypt($google_user->getId()),
                    'api_token' => Str::random(50),

                ]);
            }
            if ($user->verified == 0) {
                $user->update(['verified' => 1]);
            }
            auth()->loginUsingId($user->id);
        }
        alert()->success('', 'با موفقیت وارد شدید')->autoclose(3500)->persistent('تایید');
        return redirect(route('home'));
    }

    public function redirectToProviderGithub()
    {
        return Socialite::driver('github')->redirect();

    }

    public function handleProviderCallbackGithub()
    {
        $github_user = Socialite::driver('github')->user();
        $user = User::whereEmail($github_user->getEmail())->first();
        if ($github_user) {
            if (!$user) {
                $user = User::create([
                    'name' => $github_user->getNickname(),
                    'last_name' => ' ',
                    'avatar' => $github_user->getAvatar(),
                    'username' => Str::random(5),
                    'email' => $github_user->getEmail(),
                    'password' => bcrypt($github_user->getId())
                ]);
            }
            if ($user->verified == 0) {
                $user->update(['verified' => 1]);
            }
            auth()->loginUsingId($user->id);
        }
        alert()->success('', 'با موفقیت وارد شدید')->autoclose(3500)->persistent('تایید');
        return redirect(route('home'));
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
            'g-recaptcha-response' => 'recaptcha'

        ]);
    }
}

<?php

namespace App\Http\Controllers\UserPanel;

use App\Http\Controllers\Controller;
use App\Model\Payment;
use App\User;
use Carbon\Carbon;
use SEO;
use Illuminate\Http\Request;
use SoapClient;

class UserPanelController extends Controller
{
    protected $MerchantID = 'f83cc956-f59f-11e6-889a-005056a205be'; //Required

    public function index()
    {
        SEO::setTitle('پنل کاربری');

        return view('/UserPanel.index');
    }

    public function history(User $user)
    {
        SEO::setTitle('تاریخچه پرداخت');

        $payments = auth()->user()->payment()->paginate(20);

        return view('/UserPanel.history', compact('payments'));

    }

    public function vip(Request $request)
    {
        return view('/UserPanel.vip');

    }

    public function payment()
    {
        auth()->user()->isVip();

        $this->validate(request(), [
            'plan' => 'required'
        ]);
        switch (\request()->plan) {
            case 6:
                $price = 3000;
                break;
            case 12:
                $price = 12000;
                break;
            default:
                $price = 1000;

        }

//        $Description = 'توضیحات تراکنش تستی'; // Required
//        $Email = auth()->user()->email; // Optional
//        $CallbackURL = url(route('user.panel.checker')); // Required
//
//        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
//
//        $result = $client->PaymentRequest(
//            [
//                'MerchantID' => $this->MerchantID,
//                'Amount' => $price,
//                'Description' => $Description,
//                'Email' => $Email,
//                'CallbackURL' => $CallbackURL,
//            ]
//        );
//
//        //Redirect to URL You can do it also by creating a form
//        if ($result->Status == 100) {
//
//            auth()->user()->payment()->create([
//                'resnumber' => $result->Authority,
//                'price' => $price,
//                'course_id' => 'vip',
//                'status'=>0
//            ]);
//
//            return redirect('https://www.zarinpal.com/pg/StartPay/' . $result->Authority);
//        } else {
//            echo 'ERR: ' . $result->Status;
//        }

        $CallbackURL = url(route('user.panel.checker')); // Required
        $data = array("merchant_id" => $this->MerchantID,
            "amount" => $price,
            "callback_url" => $CallbackURL,
            "description" => "خرید تست",
            "metadata" => ["email" => auth()->user()->email],
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://api.zarinpal.com/pg/v4/payment/request.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));

        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true, JSON_PRETTY_PRINT);
        curl_close($ch);


        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            if (empty($result['errors'])) {
                if ($result['data']['code'] == 100) {
                    auth()->user()->payment()->create([
                        'resnumber' => $result['data']['authority'],
                        'price' => $price,
                        'course_id' => 'vip',
                        'status' => 0
                    ]);
                    return redirect('https://www.zarinpal.com/pg/StartPay/' . $result['data']["authority"]);
                }
            } else {
                echo 'Error Code: ' . $result['errors']['code'];
                echo 'message: ' . $result['errors']['message'];

            }
        }
    }

    public function checker()
    {
        /*$Authority = request('Authority');

        $payment = Payment::whereResnumber($Authority)->firstOrFail();


        if (request('Status') == 'OK') {
            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $this->MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $payment->price,
                ]
            );

            if ($result->Status == 100) {
                if ($this->checkPayment($payment)) {
                    alert()->success('عملیات مورد نظر با موفقیت انجام شد', 'با تشکر');
                    return redirect(route('user.panel.history'));
                }
            } else {
                echo 'Transaction failed. Status:' . $result->Status;
            }
        } else {
            echo 'Transaction canceled by user';
        }*/

        $Authority = \request('Authority');
        $payment = Payment::whereResnumber($Authority)->firstOrFail();
        $data = [
            "merchant_id" => $this->MerchantID,
            "authority" => $Authority,
            "amount" => $payment->price
        ];
        $jsonData = json_encode($data);
        $ch = curl_init('https://api.zarinpal.com/pg/v4/payment/verify.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v4');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            if ($result['data']['code'] == 100) {
                if ($this->checkPayment($payment)) {
                    alert()->success('عملیات مورد نظر با موفقیت انجام شد', 'با تشکر');
                    return redirect(route('user.panel.history'));
                }
                return 'Transation success. RefID:' . $result['data']['ref_id'];
            } else {
                return 'code: ' . $result['errors']['code'] . "<br>" . 'message: ' . $result['errors']['message'];
            }
        }
    }

    private function checkPayment($payment)
    {
        $payment->update([
            'status' => 1,
        ]);

        switch ($payment) {
            case 1000:
                $month = 1;
                break;
            case 3000:
                $month = 3;
                break;
            case 12000:
                $month = 12;
                break;
        }
        $user = $payment->user_id;
        $vipTime = $user->isVip() ? Carbon::parse($user->viptime) : Carbon::now();
        $user->update([
            'viptime' => $vipTime->addMonths($month),
        ]);
        return true;
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param null $gard
     * @return mixed
     */
    public function handle($request, Closure $next, $gard = null)
    {
        if (Auth::guard($gard)->check()){
            if (auth()->user()->isAdmin()){
//                alert()->success('با موفقیت وارد شدید', '')->autoclose(3500);
                return $next($request);
            }
        }
        alert()->error('خطا 403', '!شما اجازه دسترسی ندارید')->autoclose(3500);
        return redirect('/');
    }
}

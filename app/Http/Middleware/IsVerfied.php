<?php

namespace App\Http\Middleware;

use Closure;

class IsVerfied
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            if (auth()->user()->verified == 0) {
                auth()->logout();
                alert()->warning('حساب کاربری شما تایید نشده.', 'لینک فعالسازی به ایمیل شما ارسال شده')->autoclose(3500);
                return redirect(route('home'));
            }
        }
        return $next($request);
    }
}

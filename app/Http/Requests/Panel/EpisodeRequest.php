<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;

class EpisodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id' => 'required',
            'title' => 'required|min:2|max:191',
            'description' => 'nullable|max:191',
            'type' => 'required|string',
            'tag' => 'required|max:191',
            'fileUrl' => 'required',
            'time' => 'required',
            'number' => 'required|integer',
        ];
    }
}

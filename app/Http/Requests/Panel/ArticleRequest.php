<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required| string| min:2| max:191',
            'description' => 'required|string|min:2 |max:191',
            'body' => 'required|string|min:2',
            'tag' => 'nullable|min:0|max:191',
            'imgsUrl' => 'mimes:png,jpg,jpeg,gif',
            'time' => 'nullable|string',
        ];
    }
}

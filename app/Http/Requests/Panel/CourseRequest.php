<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:2|max:191',
            'category_id'=>'required',
            'body'=>'nullable',
            'price'=>'nullable|numeric|integer',
            'imgUrl'=>'nullable|mimes:png,jpg,jpeg,gif'
        ];
    }
}

<?php

namespace App\Jobs;

use App\Mail\VerifiedEmail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user, $code;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param $code
     */
    public function __construct(User $user,$code)
    {
        $this->user=$user;
        $this->code=$code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user)->send(new VerifiedEmail($this->user,$this->code));
    }
}

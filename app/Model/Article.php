<?php

namespace App\Model;

use App\Indexer\ArticleIndexConfigurator;
use App\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Article extends Model
{
    use Sluggable;

    protected $fillable = ['user_id', 'title', 'category_id', 'description', 'imgsUrl', 'body', 'tag', 'slug', 'viewCount', 'commentCount', 'time', 'visibility', 'lang'];


    protected $indexConfigurator = ArticleIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for model fields
    protected $mapping = [
    ];


    protected $casts = [
        'imgsUrl' => 'array'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *installation: composer require cviebrock/eloquent-sluggable ^6.*
     * @return array
     */

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function path()
    {
        return app()->getLocale() . "/article/$this->slug";
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function scopeSearch($query, $q)
    {

        $query->whereHas('category', function ($query) use ($q) {
            $query->where('category_name', 'like', '%' . $q . '%')
                ->orWhere('subject', 'like', '%' . $q . '%');
        })->orWhere('title', 'like', '%' . $q . '%')
            ->orWhere('tag', 'like', '%' . $q . '%')
            ->orWhere('description', 'like', '%' . $q . '%');

        return $query;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = [$this->title,$this->category()];

        // Customize array...

        return $array;
    }

}

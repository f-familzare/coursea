<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['permission_name', 'label'];

    public function role()
    {
        return $this->belongsToMany(Role::class);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Fatemeh
 * Date: 1/3/2021
 * Time: 1:54 AM
 */

namespace App\Model;


trait HasRole
{
    public function role()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($role)
    {
        if (is_string($role)){
            return $this->role->contains('role_name',$role);
        }
        return !! $role->intersect($this->role)->count();
    }
}
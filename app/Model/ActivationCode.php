<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ActivationCode extends Model
{
    protected $fillable = ['user_id', 'code', 'used', 'expire'];

    public function scopeCodeGenerator($query, $user)
    {
        $verificationCode = $this->generator();
        return $query->create([
            'user_id' => $user->id,
            'expire' => Carbon::now()->addMinutes(60),
            'code' => $verificationCode
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    private function generator()
    {
        do {
            $code = Str::random(50);
            $checking = static::whereCode($code)->get();
        } while (!$checking->isEmpty());
        return $code;
    }


}

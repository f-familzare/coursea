<?php

namespace App\Model;

use App\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Course extends Model
{
    use Sluggable;

    protected $fillable = ['user_id', 'title', 'category_id', 'body', 'price', 'type', 'tag', 'slug', 'imgUrl', 'viewCount', 'episodeCount', 'commentCount', 'time', 'lang'];
    protected $casts = ['imgUrl' => 'array'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function path()
    {
        return app()->getLocale() . "/course/$this->slug";
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function setBodyAttribute($value)
    {
        $this->attributes['description'] = Str::limit(preg_replace('/<[^>]*>/', '', $value), 100);
        $this->attributes['body'] = $value;
    }

    public function episode()
    {
        return $this->hasMany(Episode::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function payment()
    {
        return $this->hasMany(Payment::class);
    }

    public function scopeSearch($query, $q)
    {

        $query->whereHas('category', function ($query) use ($q) {
            $query->where('category_name', 'like', '%' . $q . '%')
                ->orWhere('subject', 'like', '%' . $q . '%');
        })->orWhere('title', 'like', '%' . $q . '%')
            ->orWhere('tag', 'like', '%' . $q . '%')
            ->orWhere('description', 'like', '%' . $q . '%');

        return $query;
    }
}

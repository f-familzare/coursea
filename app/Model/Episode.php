<?php

namespace App\Model;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Episode extends Model
{
    use Sluggable;

    protected $fillable = ['course_id', 'title', 'description', 'type', 'tag', 'slug', 'fileUrl', 'time', 'number', 'downloadCount', 'commentCount', 'viewCount'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function path()
    {
        return "{$this->course->slug}/{$this->number}";
    }

    public function download()
    {
        $status = false;
        if (!auth()->check()) return '#';
        switch ($this->type) {
            case 'free':
                $status = true;
                break;
            case 'vip':
                auth()->user()->isVip() ? $status = true : $status = false;
                break;
            case 'cash':
                auth()->user()->isLearning($this->course) ? $status = true : $status = false;
                break;
        }
        $activeTime = Carbon::now()->addHour()->timestamp;
        $hash =Hash::make('werty@uioppdfghj#klqwertyu' . $this->id . request()->ip() . $activeTime);
        return $status ? '/'.app()->getLocale()."/download/$this->id?mac=$hash&t=$activeTime" : "#";
    }
}
